package com.jpalibrary2.jpalibrary2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaLibrary2Application {

	public static void main(String[] args) {
		SpringApplication.run(JpaLibrary2Application.class, args);
	}

}
