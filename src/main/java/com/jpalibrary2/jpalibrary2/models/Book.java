package com.jpalibrary2.jpalibrary2.models;

import java.util.Set;
import java.util.HashSet;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity; // import de la structure entity pour indiquer à springboot que c'est une table à créer dans la bdd
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "BOOK")
public class Book {
    // id (Long, clé primaire auto incrémentée),
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // créer des catégories

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;
    // //@OnDelete(action = OnDeleteAction.CASCADE)
    // @JsonIgnore

    @ManyToMany
    Set<Category> categories;

    // title (String, non null,longueur maximale de 100 caractères),
    @Column(length = 100, nullable = false)
    private String title;

    // description (String, peut être null, est un texte long),
    @Column(columnDefinition = "TEXT", nullable = true)
    private String description;

    // available (Boolean, défaut à true)
    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private Boolean available = true;

    // constructeur
    public Book() {
    }

    public Book(Long id, String title, String description, Boolean available) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.available = available;
    }

    // getter et setter
    public Long getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isAvailable() {
        return this.available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Author getAuthor() {
        return this.author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

}
