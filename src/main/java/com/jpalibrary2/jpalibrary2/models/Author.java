package com.jpalibrary2.jpalibrary2.models;

import java.util.Set;
import java.util.HashSet;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "AUTHOR")
public class Author {

    public Author() {
    }
    // Ajoute les entités Author avec un identifiant (Long) auto-généré, un nom
    // (String) et un prénom (String)

    // id (Long, clé primaire auto incrémentée),
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // firstName
    @Column(columnDefinition = "TEXT", nullable = true)
    private String firstName;

    // lastName
    @Column(columnDefinition = "TEXT", nullable = true)
    private String lastName;

    // un auteur peut avoir écrit à plusieurs livres
    @OneToMany(mappedBy = "author")
    private Set<Book> books = new HashSet<>();

    public Set<Book> getBooks() {
        return this.books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
